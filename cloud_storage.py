from cloudstorage.drivers.amazon import S3Driver
from cloudstorage.drivers.microsoft import AzureStorageDriver

def save_to_cloud(provider, filepath, bucket, object='', **kwargs):
    """
    Save a file to a cloud container. Supports S3 and Azure.

    :param provider: [s3, azure]
    :param filepath: path to local file to upload
    :param bucket: name of destination bucket or container
    :param object: name of destination object, or None to use existing filename
    :param kwargs: additional arguments. s3: [key, secret, region], azure: [account, key]
    :return:
    """
    if provider == 's3':
        if 'key' not in kwargs or 'secret' not in kwargs or 'region' not in kwargs:
            raise Exception('If using s3, you must specify the `key`, `secret` and `region` parameters.')
        storage = S3Driver(key=kwargs.get('key'), secret=kwargs.get('secret'), region=kwargs.get('region'))
    elif provider == 'azure':
        if 'account' not in kwargs or 'key' not in kwargs:
            raise Exception('If using azure, you must specify the `account` and `key` parameters.')
        storage = AzureStorageDriver(account_name=kwargs.get('account'), key=kwargs.get('key'))
    else:
        raise Exception('Provider must be one of [s3, azure].')

    container = storage.get_container(bucket)
    blob = container.upload_blob(filepath, object)

    return blob


def get_from_cloud(provider, object, bucket, filepath='', **kwargs):
    """
    Retrieve a file from a cloud container. Supports S3 and Azure.

    :param provider: [s3, azure]
    :param object: name of remote object
    :param bucket: name of remote bucket or container
    :param filepath: path to local file to write to
    :param kwargs: additional arguments. s3: [key, secret, region], azure: [account, key]
    :return: local filepath
    """
    if provider == 's3':
        if 'key' not in kwargs or 'secret' not in kwargs or 'region' not in kwargs:
            raise Exception('If using s3, you must specify the `key`, `secret` and `region` parameters.')
        storage = S3Driver(key=kwargs.get('key'), secret=kwargs.get('secret'), region=kwargs.get('region'))
    elif provider == 'azure':
        if 'account' not in kwargs or 'key' not in kwargs:
            raise Exception('If using azure, you must specify the `account` and `key` parameters.')
        storage = AzureStorageDriver(account_name=kwargs.get('account'), key=kwargs.get('key'))
    else:
        raise Exception('Provider must be one of [s3, azure].')

    container = storage.get_container(bucket)
    blob = container.get_blob(object)
    local_path = filepath if filepath else object
    blob.download(local_path)

    return local_path